import hashlib
import math
import os
import random
from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import serialization, hashes
from sympy import nextprime, primitive_root

def wczytaj_wiadomosc(filename):
    with open(filename, "rb") as f1:
        message = f1.read()
    f1.close()
    return message

def create_hashed_message(message):
    hash_obj = hashlib.sha256()
    hash_obj.update(message)
    message_digest = hash_obj.digest()
    return message_digest

def RSA_wygeneruj_klucze():
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048
    )
    public_key = private_key.public_key()
    pem_private = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )

    with open("private_key.pem", "wb") as f1:
        f1.write(pem_private)
    f1.close()

    pem_public = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )

    with open("public_key.pem", "wb") as f2:
        f2.write(pem_public)
    f2.close()

def podpisz_wiadomosc(message, private_key):
    signature = private_key.sign(
        message,
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA256()
    )
    with open("podpis_cyfrowy.sig", "wb") as f1:
        f1.write(signature)
    f1.close()

def zweryfikuj_podpis(filename):
    with open("public_key.pem", "rb") as f1:
        public_key = serialization.load_pem_public_key(f1.read())
    with open("podpis_cyfrowy.sig", "rb") as f2:
        signature = f2.read()
    message = wczytaj_wiadomosc(filename)
    message_digest = create_hashed_message(message)
    try:
        public_key.verify(
            signature,
            message_digest,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
        print("Podpis cyfrowy jest poprawny.")
    except InvalidSignature:
        print("Podpis cyfrowy jest niepoprawny.")
    f1.close()
    f2.close()

def RSA_podpisz(filename):
    wiadomosc = wczytaj_wiadomosc(filename)
    hash_wiadomosc = create_hashed_message(wiadomosc)
    with open("private_key.pem", "rb") as key_file:
        private_key = serialization.load_pem_private_key(
            key_file.read(),
            password=None
        )
    podpisz_wiadomosc(hash_wiadomosc, private_key)

def El_Gamal_generate_key():
    p = nextprime(random.getrandbits(128))
    g = primitive_root(p)
    priv_key = random.getrandbits(128) % (p - 1)
    y = pow(g, priv_key, p)
    f1 = open("ElGamal_public_key.txt", "w")
    f1.write(str(p)+","+str(g)+","+str(y))
    f1.close()
    f2 = open("ElGamal_private_ket.txt", "w")
    f2.write(str(priv_key))
    f2.close()

def generate_k(p):
    k = random.getrandbits(128) % (p - 1)
    while math.gcd(k,p-1) != 1:
        k = random.getrandbits(128) % (p - 1)
    return k

def generate_sig(p,g,h,x):
    while True:
        k = generate_k(p)
        r = pow(g, k, p)
        k_inv = pow(k, -1, p - 1)
        s = (h - x * r) * k_inv % (p - 1)
        if s != 0:
            break
    return r, s

def El_Gamal_podpis(filename):
    with open("ElGamal_private_ket.txt", "r") as f1:
        x = int(f1.read().strip())
    f1.close()
    with open("ElGamal_public_key.txt", "r") as f2:
        p,g,y = list(map(int,f2.read().split(",")))
    f2.close()
    message = wczytaj_wiadomosc(filename)
    hashed_message = create_hashed_message(message)
    h = int.from_bytes(hashed_message, 'big')
    r, s = generate_sig(p,g,h,x)
    with open("ElGamal_sign.txt", "w") as f3:
        f3.write(str(r)+","+str(s))
    f3.close()

def ElGamal_wer(filename):
    message = wczytaj_wiadomosc(filename)
    hashed_message = create_hashed_message(message)
    h = int.from_bytes(hashed_message, 'big')
    with open("ElGamal_sign.txt", "r") as f3:
        podpis = f3.read()
    r,s = list(map(int,podpis.split(",")))
    with open("ElGamal_public_key.txt", "r") as f2:
        p,g,y = list(map(int,f2.read().split(",")))
    f2.close()
    if not 1 < r < p-1:
        print("Podpis nieprawidłowy")
        return
    if not 0 < s < p - 1:
        print("Podpis nieprawidłowy")
        return
    v1 = (pow(y, r, p) * pow(r, s, p)) % p
    v2 = pow(g, h, p)
    if v1 == v2:
        print("Podpis prawidłowy")
    else:
        print("Podpis nieprawidłowy")
    return

seed = int.from_bytes(os.urandom(8), 'big')
random.seed(seed)

while True:
    print("1 - wygeneruj klucze RSA 2 - podpisz dokument RSA 3 - zweryfikuj podpis RSA 4 - wygeneruj klucze ElGamal 5 - podpisz ElGamal 6 - zweryfikuj podpis ElGamal 7 - wyjdź")
    wybor = input()
    if wybor == '1':
        RSA_wygeneruj_klucze()
    if wybor == '2':
        filename = input("Podaj nazwę pliku\n")
        RSA_podpisz(filename)
    if wybor == '3':
        filename = input("Podaj nazwę pliku\n")
        zweryfikuj_podpis(filename)
    if wybor == '4':
        El_Gamal_generate_key()
    if wybor == '5':
        filename = input("Podaj nazwę pliku\n")
        El_Gamal_podpis(filename)
    if wybor == '6':
        filename = input("Podaj nazwę pliku\n")
        ElGamal_wer(filename)
    if wybor == '7':
        exit(0)