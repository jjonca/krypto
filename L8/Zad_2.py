import random
import os
import math

SYMBOL_SIZE = 128
MAX_BLOCK_SIZE = 1
WŁASNY = False

def alg_eucl(a, b):
    xa = 1
    ya = 0
    xb = 0
    yb = 1
    x = 0
    y = 0
    while not a * b == 0:
        if a >= b:
            c = a // b
            a = a % b
            xa = xa - xb * c
            ya = ya - yb * c
        else:
            c = b // a
            b = b % a
            xb = xb - xa * c
            yb = yb - ya * c
    if a > 0:
        x = xa
        y = ya
    elif b > 0:
        x = xb
        y = yb
    return a+b, x, y

def licz_m(liczba):
    count = 0
    while liczba % 2 == 0:
        liczba //= 2
        count += 1
    return (count, liczba)

def nwd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

def RM(n,k=5):
    if n <= 1:
        return False
    if n == 2:
        return True
    if n == 3:
        return True
    if n % 2 == 0:
        return False
    r, d = licz_m(n - 1)
    for i in range(k):
        a = random.randrange(2, n - 1)
        x = pow(a, d, n)
        if x == 1 or x == n - 1:
            continue
        for i in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

def isPrime(num):
    if not RM(num):
        return False
    return True


def prime_generator(n):
    while True:
        num = random.randrange(2 ** (n - 1), 2 ** (n))
        if isPrime(num):
            return num

def generate_e(p, q, n):
    while True:
        e = random.randrange(2 ** (n - 1), 2 ** (n))
        if nwd(e, (p-1)*(q-1)) == 1:
            return e

def generate_d(e,p,q):
    d = alg_eucl(e,(p-1)*(q-1))[1]
    if d < 0:
        d = (p-1)*(q-1) - d
    return d
def generate_key(n):
    p = prime_generator(n)
    q = prime_generator(n)
    while q == p:
        q = prime_generator(n)
    pq = p * q
    e = generate_e(p, q, n)
    d = generate_d(e, p, q)
    return pq,e,d

def zapisz_klucz_do_pliku(n,pq, e, d):
    f1 = open("klucz_publiczny.txt","w")
    f2 = open("klucz_prywatny.txt","w")
    f1.write(str(n) + "," + str(pq) + "," + str(e))
    f2.write(str(n) + "," + str(pq) + "," + str(d))
    f1.close()
    f2.close()

def wczytaj_klucz_publiczny():
    f1 = open("klucz_publiczny.txt", "r")
    n,pq,e = list(map(int,(f1.readline().strip().split(","))))
    return n, pq,e

def create_block(message):
    blocked_message = []
    blocked_message2 = []
    for i in range(1,len(message)+1):
        blocked_message.append(ord(message[i-1]))
        if i % MAX_BLOCK_SIZE == 0 and i > 0:
            blocked_message2.append(blocked_message)
            blocked_message = []
    if len(message) % MAX_BLOCK_SIZE != 0:
        blocked_message2.append(blocked_message)
    return blocked_message2

def create_summed_message(blocked_message):
    summed_message = []
    summed_message_block = []
    sum = 0
    i = 0
    for elem in blocked_message:
        for elem2 in elem:
            sum += elem2*SYMBOL_SIZE**i
            i += 1
        i = 0
        summed_message_block.append(sum)
        sum = 0
        summed_message.append(summed_message_block)
        summed_message_block = []
    return summed_message

def zasz_block(summed_message_block, e, n):
    return pow(summed_message_block[0],e,n)

def zasz(summed_message,e,n):
    zasz_wiad_list = []
    for block in summed_message:
        zasz_wiad_list.append(zasz_block(block, e, n))
    zasz_message = ",".join(map(str,zasz_wiad_list))
    f1 = open("zasz_tekst.txt", "w")
    f1.write(zasz_message)
    f1.close()

def wczytaj_klucz_prywatny():
    f1 = open("klucz_prywatny.txt", "r")
    n,pq,d = list(map(int,(f1.readline().strip().split(","))))
    return n, pq,d

def dec_block(message_block,d,n):
    return pow(message_block, d, n)

def dec_block_in_message(block, ostatni):
    dec_block_litery = []
    dl_bloku = MAX_BLOCK_SIZE
    if ostatni:
        i2 = 0
        for i in range(0,MAX_BLOCK_SIZE+1):
            litera = block//SYMBOL_SIZE**i
            if litera <32 or litera>SYMBOL_SIZE:
                continue
            else:
                i2=i+1
                dl_bloku = i2
                break
    for i in range(dl_bloku-1,-1,-1):
        litera = chr(block//SYMBOL_SIZE**i)
        block = block % SYMBOL_SIZE**i
        dec_block_litery.append(litera)
    reversed_dec_block_litery = dec_block_litery[::-1]
    return reversed_dec_block_litery

def dec(zasz_message,d,n):
    dec_message = []
    dec_message2 = []
    zasz_message_blocks = zasz_message.split(",")
    dec_blocks = []
    for block in zasz_message_blocks:
        dec_blocks.append(dec_block(int(block),d,n))
    for i in range(len(dec_blocks)):
        if i == len(dec_blocks) - 1:
         dec_message.append(dec_block_in_message(dec_blocks[i], True))
        else:
            dec_message.append(dec_block_in_message(dec_blocks[i], False))
    for elem in dec_message:
        dec_message2.extend(elem)
    return "".join(dec_message2)
def program():
    global MAX_BLOCK_SIZE
    global WŁASNY
    while True:
        wybor = input("Co chcesz zrobić? 1 - wygeneruj klucz 2 - zaszyfruj 3 - odszyfruj 4 - podaj własną długość bloku (min 1) 5 - wyjdź\n")
        if wybor == '1':
            n = int(input("Podaj dlugosc klucza\n"))
            if n < 2:
                print("Za krótki\n")
            else:
                pq, e, d = generate_key(n)
                zapisz_klucz_do_pliku(n,pq,e,d)
        if wybor == '2':
            filename = input("podaj nazwę pliku\n")
            with open(filename,"r") as f1:
                message = f1.read()
            n,pq,e = wczytaj_klucz_publiczny()
            max_size = int(int(n)//math.log(SYMBOL_SIZE,2))
            if MAX_BLOCK_SIZE > max_size:
                print("Błąd. Zmniejsz długość bloku lub wygeneruj dluzszy klucz")
            else:
                if not WŁASNY:
                    MAX_BLOCK_SIZE = max_size
                blocked_message = create_block(message)
                summed_message = create_summed_message(blocked_message)
                zasz(summed_message,e,pq)
        if wybor == '3':
            filename = input("podaj nazwę pliku\n")
            with open(filename, "r") as f1:
                zasz_message = f1.read()
            n, pq, d = wczytaj_klucz_prywatny()
            max_size = int(int(n) // math.log(SYMBOL_SIZE, 2))
            if MAX_BLOCK_SIZE > max_size:
                print("Błąd. Zmniejsz długość bloku lub wygeneruj dluzszy klucz")
            else:
                if not WŁASNY:
                    MAX_BLOCK_SIZE = max_size
                f2 = open("odsz_tekst.txt","w")
                odsz_message = dec(zasz_message,d,pq)
                f2.write(odsz_message)
                f2.close()
        if wybor == '4':
            dl = int(input())
            if dl >= 1:
                MAX_BLOCK_SIZE = dl
                WŁASNY = True
            else:
                print("Za krotki")
        if wybor == '5':
            exit(0)

def main():
    seed = int.from_bytes(os.urandom(8), 'big')
    random.seed(seed)
    program()

if __name__ == "__main__":
    main()