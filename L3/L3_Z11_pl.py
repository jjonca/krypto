import string
from itertools import product
import re

FREQ = ['I', 'A', 'E', 'O', 'Z', 'N', 'S', 'C', 'R', 'W', 'Y', 'Ł', 'D', 'K', 'M', 'T', 'P', 'U', 'J', 'L', 'G', 'Ę', 'B', 'Ą', 'H', 'Ż', 'Ś', 'Ó', 'Ć', 'Ń', 'F', 'Ź', 'V', 'Q', 'X']
najcz_polski = ('I', 'A', 'E', 'O', 'Z', 'N')
najrz_polski = ('Ń', 'F', 'Ź', 'V', 'Q', 'X')
ALPHABET_DUZE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZĄĆĘŁŃÓŚŹŻ'
ALPHABET_MALE = 'abcdefghijklmnopqrstuvwxyząćęłńóśźż'

ALFABET_ASCII = {}
ALFABET_DEKOD = {}

i = 0
for letter in ALPHABET_DUZE:
    ALFABET_ASCII[letter] = i
    i += 1

for key in ALFABET_ASCII.keys():
    ALFABET_DEKOD[ALFABET_ASCII[key]] = key

def wczytaj():
    dane = []
    try:
        while True:
            line = input().strip()
            if not line:
                break
            dane.append(line)
    except EOFError:
        pass
    dane = ''.join(dane)
    return dane

def strip_tekst(tekst):
    lista_liter = []
    for litera in tekst:
        litera = litera.upper()
        if litera in FREQ:
            lista_liter.append(litera)
    tekst_stripped = ''.join(lista_liter)
    return tekst_stripped

def znajdz_powt(tekst):
    sekwencje = {}
    n = len(tekst)
    for i in range(0, n - 7 + 1):
        for dlugosc in range(3, 7):
            sekwencja = tekst[i:i + dlugosc]
            if sekwencja in sekwencje:
                sekwencje[sekwencja][0] += 1
                sekwencje[sekwencja].append(i)
            else:
                sekwencje[sekwencja] = [1, i]
    powt_sekw = {}
    for key in sekwencje:
        if sekwencje[key][0] > 1:
            powt_sekw[key] = sekwencje[key]
    return powt_sekw


def rozklad(liczba):
    czynniki = []
    dzielnik = 3
    while liczba > 1 and dzielnik < 7:
        if liczba % dzielnik == 0:
            czynniki.append(dzielnik)
        dzielnik += 1
    return czynniki


def Kasinski(tekst):
    powt = znajdz_powt(tekst)
    dict_powt = {}
    for key in powt:
        dict_powt[key] = []
        for i in range(len(powt[key]) - 1, 0, -1):
            for j in range(i - 1, 0, -1):
                odstep = powt[key][i] - powt[key][j]
                dict_powt[key].append(odstep)

    dict_czynniki = {}

    for key in dict_powt:
        dict_czynniki[key] = []
        for value in dict_powt[key]:
            czynniki = rozklad(value)
            dict_czynniki[key].extend(czynniki)

    dict_count = {}

    for key in dict_czynniki:
        dict_count_values = {}
        for value in dict_czynniki[key]:
            if value not in dict_count_values.keys():
                dict_count_values[value] = 1
            else:
                dict_count_values[value] += 1
        dict_count_values = dict(sorted(dict_count_values.items(), key=lambda x: x[1]))
        dict_count[key] = dict_count_values

    wynik = []

    for key in dict_count:
        for value in dict_count[key]:
            wynik.append(value)

    wynik = set(wynik)
    wynik = list(wynik)
    wynik.sort()
    return wynik

def build_substrings(tekst, value):
    powt_dict = {key: [] for key in range(0, value)}
    for i in range(0, len(tekst)-value, value):
        for j in range(i, value+i):
            powt_dict[j % value].extend(tekst[j])
    for key in powt_dict:
        powt_dict[key] = ''.join(powt_dict[key])
    return powt_dict

def decrypt(M, K):

    index_przes = []
    for litera in K:
        kod_litera = ALFABET_ASCII[litera]
        index_przes.append(kod_litera)

    slowo = []
    n = len(index_przes)
    i = 0

    for litera in M:
        if litera.upper() in FREQ:
            if litera.isupper():
                kod_litera = ALFABET_ASCII[litera]
                kod_litera = (kod_litera - index_przes[i]) % 35
                litera = ALFABET_DEKOD[kod_litera]
                i = (i + 1) % n
            else:
                kod_litera = ALFABET_ASCII[litera.upper()]
                kod_litera = (kod_litera - index_przes[i]) % 35
                litera = ALFABET_DEKOD[kod_litera].lower()
                i = (i + 1) % n
        slowo.append(litera)
    slowo = ''.join(slowo)
    return slowo

def decrypt_list(powt):
    powt_dict_subst = {litera: 0 for litera in FREQ[::-1]}
    for litera in FREQ:
        dec_slowo = decrypt(powt, litera)
        powt_dict_subst[litera] = dec_slowo
    return powt_dict_subst

def analiza_czest(tekst):
    czest_dict = {litera: 0 for litera in FREQ[::-1]}
    analiza_czest = {'najcz': 0, 'najrz': 0}
    for litera in tekst:
        if litera in FREQ:
            czest_dict[litera.upper()] = czest_dict[litera.upper()] + 1

    dict_sort = sorted(czest_dict.items(), key=lambda x: x[1], reverse=True)

    najcz_szyfr = dict_sort[0:6]
    najrz_szyfr = dict_sort[-6:]

    najrz_szyfr = dict(najrz_szyfr)
    najcz_szyfr = dict(najcz_szyfr)

    for litera in najcz_szyfr:
        if litera in najcz_polski:
            analiza_czest['najcz'] = analiza_czest['najcz'] + 1

    for litera in najrz_szyfr:
        if litera in najrz_polski:
            analiza_czest['najrz'] = analiza_czest['najrz'] + 1

    czestotliwosc = analiza_czest['najrz'] + analiza_czest['najcz']
    return czestotliwosc

def zbadaj_czestotliwosc(lista_ciagow_tekst):
    for ciag in lista_ciagow_tekst:
        lista_ciagow_tekst[ciag] = analiza_czest(lista_ciagow_tekst[ciag])
    lista_ciagow_tekst = sorted(lista_ciagow_tekst.items(), key=lambda x: x[1], reverse=True)
    return lista_ciagow_tekst

def build_lista(tekst_stripped, dl_klucza):
    lista_liter = []
    powt_dict = build_substrings(tekst_stripped, dl_klucza)
    for key in powt_dict:
        powt_dict_subst = decrypt_list(powt_dict[key])
        lista_liter.append(zbadaj_czestotliwosc(powt_dict_subst))
    return lista_liter


plik = open("polski.txt", "r", encoding='UTF-8')

slownik = {}
slowo = plik.readline()
while slowo:
    slowo = slowo.rstrip('\n')
    slowo = slowo.upper()
    slownik[slowo] = slowo[0]
    slowo = plik.readline()


tekst = wczytaj()
tekst_stripped = strip_tekst(tekst)
powtorzenia = Kasinski(tekst_stripped)

pattern = r'[,;.\s]+'

for value in powtorzenia:
    lista_liter = build_lista(tekst_stripped, value)
    for indexes in product(range(6), repeat=value):
        key = ''
        for i in range(value):
            key += lista_liter[i][indexes[i]][0]
        if key in slownik:
            tekst_decrypt = decrypt(tekst, key)
            tekst_decrypt2 = re.split(pattern, tekst_decrypt)
            count = 0
            for i in range(0, 15):
                if tekst_decrypt2[i].upper() in slownik:
                    count += 1
            if count > 8:
                print(tekst_decrypt)
                print(key.lower())
                exit()


