import socket
import hashlib
import secrets
import time
import csv

print("open")
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('127.0.0.1', 12345)
print('Connecting to {} port {}'.format(*server_address))
client_socket.connect(server_address)


wybor = input("1 - logowanie 2 - nowe konto 3 - test\n")
haslo = ''
if wybor == '1' or wybor == '2':
    nazwa_uz = input("Podaj nazwe uzytkownika\n")
    haslo = input("Podaj haslo\n")
    client_socket.sendall((wybor.encode()))
    client_socket.sendall(nazwa_uz.encode())

if wybor != '1' and wybor != '2' and wybor != '3':
    client_socket.close()
    exit()

if wybor == '2':
    istnieje = client_socket.recv(1024).decode()
    if istnieje == 'True':
        print("Uzytkownik o podanej nazwie istnieje")
        client_socket.close()
        exit()
    if istnieje == 'False':
        print("Dodajemy do bazy")
    sol = secrets.token_hex(16)
    haslo_sol = (haslo + sol).encode()
    haslo_hash = hashlib.sha256(haslo_sol).hexdigest()
    print("Wysylamy haslo")
    client_socket.sendall((haslo_hash.encode()))
    client_socket.sendall(sol.encode())
    odpowiedz = client_socket.recv(1024).decode()
    print(odpowiedz)
    client_socket.close()
    exit()

if wybor == '1':
    istnieje = client_socket.recv(1024).decode()
    if istnieje == 'False':
        print("Uzytkownik o podanej nazwie nie istnieje")
        client_socket.close()
        exit()
    sol = client_socket.recv(1024)
    haslo_sol = (haslo + sol.decode()).encode()
    haslo_hash = hashlib.sha256(haslo_sol).hexdigest()
    client_socket.sendall(haslo_hash.encode())
    odpowiedz = client_socket.recv(1024).decode()
    print(odpowiedz)
    client_socket.close()
    exit()

if wybor == '3':
    csv_file1 = open('baza_dobre.csv', 'r', newline='')
    csv_file2 = open('baza_zla.csv', 'r', newline='')
    csv_file3 = open('baza_klucze.csv', 'r', newline='')
    wynik = open('wynik1.csv', 'w', newline='')
    csv_reader1 = csv.reader(csv_file1)
    csv_reader2 = csv.reader(csv_file2)
    csv_reader3 = csv.reader(csv_file3)
    csv_writer1 = csv.writer(wynik)
    csv_writer1.writerow(['poprawne?','dlugosc','czas'])
    client_socket.sendall((wybor.encode()))
    for i in range(3):
        if i == 0:
            csv_reader = csv_reader1
        if i == 1:
            csv_reader = csv_reader2
        if i == 2:
            csv_reader = csv_reader3
        for row in csv_reader:
            nazwa_uz = row[0]
            haslo = row[1]
            czas_pocz = time.time()
            client_socket.sendall(nazwa_uz.encode())
            istnieje = client_socket.recv(1024).decode()
            sol = client_socket.recv(1024)
            haslo_sol = (haslo + sol.decode()).encode()
            haslo_hash = hashlib.sha256(haslo_sol).hexdigest()
            client_socket.sendall(haslo_hash.encode())
            odpowiedz = client_socket.recv(1024).decode()
            print(odpowiedz)
            czas_kon = time.time()
            czas_dzial = czas_kon - czas_pocz
            print("Czas wykonania:", czas_dzial, "sekundy")
            if i == 0:
                poprawne = 'tak'
            else:
                poprawne = 'nie'
            row = [poprawne,len(haslo),czas_dzial]
            print(nazwa_uz,haslo)
            csv_writer1.writerow(row)
            client_socket.sendall('tak'.encode())
    client_socket.sendall('nie'.encode())

client_socket.close()