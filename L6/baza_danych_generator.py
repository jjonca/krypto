import random
import string
import csv
import hashlib

dlugosc = 0
proby = 5

csv_file = open('baza_dobre.csv', 'w', newline='')
csv_file2 = open('baza_zla.csv', 'w', newline='')
csv_file3 = open('baza_klucze.csv', 'w', newline='')
csv_writer = csv.writer(csv_file)
csv_writer2 = csv.writer(csv_file2)
csv_writer3 = csv.writer(csv_file3)

row1 = ['user', '1234']
csv_writer.writerow(row1)

for i in range(0, 7, 1):
    dlugosc = i
    if i == 0:
        dlugosc = 1
    for j in range(proby):
        uzytkownik = ''.join(random.choices(string.ascii_letters + string.digits, k=5))
        haslo = ''.join(random.choices(string.ascii_letters + string.digits, k=dlugosc))
        haslo2 = ''.join(random.choices(string.ascii_letters + string.digits, k=dlugosc))
        row1 = [uzytkownik, haslo]
        row2 = [uzytkownik, haslo2]
        row3 = [uzytkownik, hashlib.sha256(haslo.encode()).hexdigest()]
        csv_writer.writerow(row1)
        csv_writer2.writerow(row2)
        csv_writer3.writerow(row3)

