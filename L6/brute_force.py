import hashlib
import itertools
import csv
import string
import time

baza_danych = {}
csv_file = open('baza_dobre.csv', 'r', newline='')
csv_file2 = open('wynik_bruteforce.csv', 'w', newline='')
csv_reader = csv.reader(csv_file)
csv_writer = csv.writer(csv_file2)
mozliwe_znaki = string.ascii_letters + string.digits
print(mozliwe_znaki)

for row in csv_reader:
    uzytkownik = row[0]
    haslo = row[1]
    haslo_hash = hashlib.sha256(haslo.encode()).hexdigest()
    baza_danych[uzytkownik] = {'hash': haslo_hash, 'haslo': haslo}
print(baza_danych)

jest = False

for uzytkownik in baza_danych.keys():
    haslo_hash = baza_danych[uzytkownik]['hash']
    jest = False
    czas_pocz = time.time()
    for dlugosc_hasla in range(1, 7):
        if not jest:
            for haslo2 in itertools.product(mozliwe_znaki, repeat=dlugosc_hasla):
                haslo2_str = ''.join(haslo2)
                haslo_hash2 = hashlib.sha256(haslo2_str.encode()).hexdigest()
                if haslo_hash2 == haslo_hash:
                    jest = True
                    print(haslo2_str)
                    czas_kon = time.time()
                    czas_dzial = czas_kon - czas_pocz
                    row = [len(haslo2),czas_dzial]
                    csv_writer.writerow(row)