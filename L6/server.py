import socket
import hashlib
import secrets
import csv

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('127.0.0.1', 12345)
print('Inicjalizacja', server_address)
server_socket.bind(server_address)
server_socket.listen(1)

baza_danych = {}
csv_file = open('baza_dobre.csv', 'r', newline='')
csv_reader = csv.reader(csv_file)

for row in csv_reader:
    uzytkownik = row[0]
    haslo = row[1]
    sol = secrets.token_hex(16)
    haslo_sol = (haslo + sol).encode()
    haslo_hash = hashlib.sha256(haslo_sol).hexdigest()
    baza_danych[uzytkownik] = {'hash': haslo_hash, 'sol': sol}

kontynuuj = 'tak'

while True:
    print('Czekam na nazwiazanie poloczenia...')
    connection, client_address = server_socket.accept()
    print('Polaczono', client_address)

    while True:
        wybor = connection.recv(1024).decode()

        if wybor == '1':
            uzytkownik = connection.recv(1024).decode()
            print('Odebrano nazwe uzytkownika', uzytkownik)
            if uzytkownik in baza_danych:
                sol = baza_danych[uzytkownik]['sol']
                print('Wysylam sol', sol)
                connection.sendall('True'.encode())
                connection.sendall(sol.encode())
            else:
                connection.sendall('False'.encode())
                break
            haslo_hash = connection.recv(1024).decode()
            print('Odebrane haslo hash::', haslo_hash)
            if haslo_hash:
                if baza_danych[uzytkownik]['hash'] == haslo_hash:
                    connection.sendall('Uwierzytelnienie poprawne'.encode())
                    break
                else:
                    connection.sendall("Zla nazwa uzytkownika lub haslo".encode())
                    break
            else:
                break
        if wybor == '2':
            uzytkownik = connection.recv(1024).decode()
            print('Odebrano nazwe uzytkownika:', uzytkownik)
            if uzytkownik in baza_danych:
                connection.sendall('True'.encode())
                break
            else:
                connection.sendall('False'.encode())
            haslo_hash = connection.recv(1024).decode()
            sol = connection.recv(1024).decode()
            baza_danych[uzytkownik] = {'hash': haslo_hash, 'sol': sol}
            connection.sendall('Dodano nowego uzytkownika'.encode())
            break

        if wybor == '3':
            while kontynuuj == 'tak':
                uzytkownik = connection.recv(1024).decode()
                print('Odebrano nazwe uzytkownika', uzytkownik)
                if uzytkownik in baza_danych:
                    sol = baza_danych[uzytkownik]['sol']
                    print('Wysylam sol', sol)
                    connection.sendall('True'.encode())
                    connection.sendall(sol.encode())
                else:
                    connection.sendall('False'.encode())
                haslo_hash = connection.recv(1024).decode()
                print('Odebrane haslo hash::', haslo_hash)
                if haslo_hash:
                    if baza_danych[uzytkownik]['hash'] == haslo_hash:
                        connection.sendall('Uwierzytelnienie poprawne'.encode())
                    else:
                        connection.sendall("Zla nazwa uzytkownika lub haslo".encode())
                kontynuuj = connection.recv(1024).decode()
