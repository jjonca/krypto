import stegano
import os
import glob
import time
from stegano import lsb
from stegano.lsb import generators

def ukryj_red(filename, secret, rozmiar):
    try:
        print(filename)
        image = stegano.red.hide(filename,secret)
        os.makedirs('./out/red/'+str(rozmiar)+'/in', exist_ok=True)
        image.save('./out/red/'+str(rozmiar)+'/'+filename)
        image.close()
    except Exception as e:
        print("Błąd:", e)

def ukryj_lsb(filename, message,rozmiar):
    try:
        print(filename)
        image = lsb.hide(filename,message,None,0,"UTF-8",True)
        os.makedirs('./out/lsb/'+str(rozmiar)+'/in', exist_ok=True)
        image.save('./out/lsb/'+str(rozmiar)+'/'+filename)
        image.close()
    except Exception as e:
        print("Błąd:", e)


def ukryj_fib(filename, message, gen,met,rozmiar):
    try:
        image = lsb.hide(filename,message,gen,0,"UTF-8",True)
        path = './out/' + met +'/'+str(rozmiar)+'/in/'
        os.makedirs(path, exist_ok=True)
        image.save('./out/' + met +'/'+str(rozmiar)+'/'+ filename)
        image.close()
    except Exception as e:
        print("Błąd:", e)


def metody(count, filename, secret,rozmiar):
    met = {1:'piksel', 2:'lsb', 3:'lsb_sito', 4:'Ackermann'}
    if count == 1:
        ukryj_red(filename, secret,rozmiar)
    if count == 2:
        ukryj_lsb(filename, secret,rozmiar)
    if count == 3:
        ukryj_fib(filename, secret, generators.eratosthenes(),met[count],rozmiar)
    if count == 4:
        ukryj_fib(filename, secret, generators.ackermann(1),met[count],rozmiar)
    return met[count]

def ukryj():
    wynik = open("wynik.txt", "w")
    sciezka_katalog_in = './in'
    sciezka_pliki = glob.glob(os.path.join(sciezka_katalog_in, '*'))
    sciezka_katalog_mes = './mes'
    sciezka_pliki_mes = glob.glob(os.path.join(sciezka_katalog_mes, '*'))
    for i in range(len(sciezka_pliki)):
        wynik.write("rozmiar_pliku \t")
        plik = sciezka_pliki[i]
        rozmiar = os.path.getsize(plik)
        wynik.write(str(rozmiar)+'\n')
        for j in range(len(sciezka_pliki_mes)):
            mes = sciezka_pliki_mes[j]
            file= open(mes, 'r')
            secret = file.read()
            rozmiar = len(secret)
            file.close()
            wynik.write("dlugosc_wiadomosci \t")
            wynik.write(str(len(secret)) + '\n')
            for k in range(1, 5):
                start = time.time()
                print(k,plik,secret,rozmiar)
                metoda = metody(k, plik,secret,rozmiar)
                end = time.time()
                czas = end - start
                wynik.write(metoda+"\t")
                wynik.write(str(czas) + '\n')
    wynik.close()


if __name__ == "__main__":
        ukryj()