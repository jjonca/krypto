import re
import sys

def konwertuj_na_bity(mess):
    bity_lista = []
    for litera in mess:
        decimal = int(litera, 16)
        binary = format(decimal, '04b')
        bity_lista.append(binary)
    return bity_lista

def wczytaj_mess():
    with open('mess.txt', "r") as f1:
        mess = f1.read()
        bity_lista = konwertuj_na_bity(mess)
    bity_do_zan = "".join(bity_lista)
    return bity_do_zan

def wczytaj_cover():
    f1 = open('cover.html', "r")
    plik = f1.read()
    f1.close()
    return plik

def write_watermark(plik_lines):
    f1 = open('watermark.html',"w")
    for line in plik_lines:
        f1.write(line)
    f1.close()

def dodaj_spacje(plik, message):
    plik_lines = plik.splitlines()
    for i in range(len(message)):
        if message[i] == '1':
            plik_lines[i] = plik_lines[i] + ' \n'
        else:
            plik_lines[i] = plik_lines[i] + '\n'
    for i in range(len(message), len(plik_lines)):
        plik_lines[i] = plik_lines[i] + '\n'
    return plik_lines

def dodaj_podwojne_spacje(plik, message):
    splitted_text = plik.split(" ")
    for i in range(0, len(message)):
        if message[i] == '1':
            splitted_text[i] = splitted_text[i] + '  '
        else:
            splitted_text[i] = splitted_text[i] + ' '
    for i in range(len(message), len(splitted_text)):
        splitted_text[i] = splitted_text[i] + ' '
    return splitted_text

def znajdz_znacznik(plik):
    div_pattern = re.compile(r'(<div[^>]*>.*?</div>)')
    divs = div_pattern.findall(plik)
    count = 0
    for div in divs:
        style_pattern = re.compile(r'style="([^"]*)"')
        style_match = style_pattern.search(div)
        if style_match:
            style_content = style_match.group(1)
            if not re.search(r'\bmargin-bottom\b', style_content):
                if not re.search(r'\bline-height\b', style_content):
                    count += 1
        else:
            count +=1
    return count

def dodaj_znacznik(plik, message):
    div_pattern = re.compile(r'(<div[^>]*>.*?</div>)')
    style_pattern = re.compile(r'style="([^"]*)"')
    divs = div_pattern.findall(plik)
    modified_plik = plik
    i = 0
    for div in divs:
        style_match = style_pattern.search(div)
        if style_match:
            style_content = style_match.group(1)
            if not re.search(r'\bmargin-bottom\b', style_content):
                if not re.search(r'\bline-height\b', style_content):
                    if message[i] == '0':
                        new_style_content = style_content + "; margin-botom: 0cm"
                    else:
                        new_style_content = style_content + "; lineheight: 100%"
                    new_div = re.sub(r'style="([^"]*)"', f'style="{new_style_content}"', div)
        else:
            if message[i] == '0':
                new_div = re.sub(r'<div', r'<div style="margin-botom: 0cm"', div)
            else:
                new_div = re.sub(r'<div', r'<div style="lineheight: 100%"', div)
        modified_plik = modified_plik.replace(divs[i], new_div, 1)
        i += 1
        if i>=len(message):
            break
    return modified_plik

def znajdz_znacznik_a(plik):
    a_pattern = re.compile(r'(<a[^>]*>.*?</a>)')
    a_patterns = a_pattern.findall(plik)
    return len(a_patterns)

def dodaj_znacznik_a(plik, message):
    a_pattern = re.compile(r'(?<!<a>)<a[^>]+>[^<]*?(?!</a><a>)</a>')
    modified_plik = plik
    for i in range(len(message)):
        if message[i] == '1':
            new_plik = a_pattern.sub(r'<a>\g<0></a>', modified_plik, 1)
            modified_plik = new_plik
        if message[i] == '0':
            new_plik = a_pattern.sub(r'\g<0><a></a>', modified_plik, 1)
            modified_plik = new_plik
    return modified_plik

def zanurz(message, metoda):
    plik = wczytaj_cover()
    if metoda == '-1':
        plik = plik.replace(' \n','\n')
        liczba_wierszy = plik.count('\n')
        if liczba_wierszy < len(message):
            print("Niewystarczająca liczba wierszy.")
            exit(1)
        plik_lines = dodaj_spacje(plik,message)
        write_watermark(plik_lines)
    if metoda == '-2':
        plik = re.sub(r'\s{2,}', ' ', plik)
        liczba_spacji = plik.count(' ')
        if liczba_spacji < len(message):
            print("Niewystarczająca liczba spacji.")
            exit(1)
        plik_double_lines = dodaj_podwojne_spacje(plik, message)
        write_watermark(plik_double_lines)
    if metoda == '-3':
        plik = plik.replace('margin-botom', 'margin-bottom')
        plik = plik.replace('lineheight', 'line-height')
        plik = plik.replace('margin-bottom: 0cm; line-height: 100%', '')
        plik = plik.replace('style = ""', '')
        znacz_akapit = znajdz_znacznik(plik)
        if znacz_akapit < len(message):
            print("Niewystarczająca liczba div.")
            exit(1)
        plik_mod = dodaj_znacznik(plik,message)
        write_watermark(plik_mod)
    if metoda == '-4':
        plik = re.sub(r'<a><a[^>]+>',r'a[^>]+>',plik)
        plik = re.sub(r'</a></a>', r'</a>',plik)
        plik = re.sub(r'<a[^>]+>.*?</a><a></a>', r'<a[^>]*>.*?</a>',plik)
        plik = re.sub(r'<a>(.*?)</a>', r'\1',plik)
        znacz_a = znajdz_znacznik_a(plik)
        if znacz_a < len(message):
            print("Niewystarczająca liczba a.")
            exit(1)
        plik_mod = dodaj_znacznik_a(plik, message)
        write_watermark(plik_mod)

def open_watermark_met1():
    message = []
    with open('watermark.html', "r") as f1:
        for line in f1:
            if line.count(' \n') == 1:
                message.append('1')
            else:
                message.append('0')
    return message

def open_watermark_met2():
    f1 = open('watermark.html', "r")
    plik = f1.read()
    message = []
    patterns = re.findall(r'( +)',plik)
    for pat in patterns:
        if pat == '  ':
            message.append('1')
        else:
            message.append('0')
    return message

def open_watermark_met3():
    f1 = open('watermark.html', "r")
    plik = f1.read()
    message = []
    div_pattern = re.compile(r'(<div[^>]*>.*?</div>)')
    botom = re.compile(r'\bmargin-botom\b')
    lineheight = re.compile(r'\blineheight\b')
    divs = div_pattern.findall(plik)
    for div in divs:
        if re.search(botom, div):
            message.append('0')
        if re.search(lineheight,div):
            message.append('1')
    return message

def open_watermark_met4():
    f1 = open('watermark.html', "r")
    plik = f1.read()
    message = []
    a_pattern = re.compile(r'</a></a>|</a><a></a>')
    a_patterns = a_pattern.findall(plik)
    for i in range(len(a_patterns)):
        if a_patterns[i].startswith('</a></a>'):
            message.append('1')
        if a_patterns[i].startswith('</a><a></a>'):
            message.append('0')
    return message

def open_watermark(metoda):
    message = []
    if metoda == '-1':
        message = open_watermark_met1()
    if metoda == '-2':
       message = open_watermark_met2()
    if metoda == '-3':
        message = open_watermark_met3()
    if metoda == '-4':
        message = open_watermark_met4()
    return message

def message_convert(message):
    bit_wiad = ""
    converted_message = []
    for i in range(0, len(message),4):
        if i+4 > len(message):
            break
        for j in range(i,i+4):
            bit_wiad += message[j]
        if len(bit_wiad) % 4 != 0:
            break
        decimal = int(bit_wiad, 2)
        hexadeximal = hex(decimal)[2:]
        converted_message.append(hexadeximal)
        bit_wiad = ""
    index_zero = len(converted_message)
    for i in range(len(converted_message)-1,-1,-1):
        if converted_message[i] == '0':
            index_zero -= 1
        else:
            break
    converted_message = "".join(converted_message[:index_zero])
    return converted_message

def zapisz_wynurzony(converted_message):
    f1 = open('detect.txt', "w")
    f1.write(converted_message)
    f1.close()

def wynurz(metoda):
    message = open_watermark(metoda)
    converted_message = message_convert(message)
    zapisz_wynurzony(converted_message)

def main():
    if len(sys.argv) < 3:
        print("Wypisz: stegano.py -e/-d -1/2/3/4")
        print("e - zanurz -d wynurz")
        print("1 - dodatkowa spacja na koncu 2 - druga spacja 3 - znacznik div 4 - sekwencje otwierające/zamykające")
        sys.exit(1)

    opcja = sys.argv[1]
    metoda = sys.argv[2]

    if opcja == '-e':
        message = wczytaj_mess()
        zanurz(message, metoda)
    elif opcja == '-d':
        wynurz(metoda)
    else:
        print("Niepoprawny argument. e - zanurz -d wynurz")
        sys.exit(1)

if __name__ == "__main__":
    main()