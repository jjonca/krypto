from PIL import Image
import io
import secrets

klucz = "10101010"
IV = "111111111111"
permutacja = tuple(map(int, "01323245"))

sbox1 = "101 010 001 110 011 100 111 000 001 100 110 010 000 111 101 011"
sbox1 = sbox1.split(" ")

sbox2 = "100 000 110 101 111 001 011 010 101 011 000 111 110 010 001 100"
sbox2 = sbox2.split(" ")
rundy = "8"

def pbm_conv(obrazek_b):

    obrazek_lista = obrazek_b.split()
    width = int(obrazek_lista[1].decode())
    height = int(obrazek_lista[2].decode())
    bytes_per_row = width // 8
    if width % 8 != 0:
        filler = 8 - width % 8
    else:
        filler = 0
    obrazek_bitlist = []
    lista_bitow = obrazek_lista[-1]

    for i in range(0, len(lista_bitow), bytes_per_row + 1):
        row = []
        for j in range(bytes_per_row):
            row.append(format(lista_bitow[j + i], '08b'))
        if filler != 0:
            row.append(format(lista_bitow[i + bytes_per_row], '08b')[:width % 8])
        obrazek_bitlist.append(row)

    obrazek_bitlist0 = ''
    for entry in obrazek_bitlist:
        entry2 = ''.join(entry)
        obrazek_bitlist0 += entry2

    return (width, height, obrazek_bitlist0)


def oblicz_IV(nonce, counter):
    IV = ''
    for bajt in nonce:
        IV += format(bajt, '08b')
    IV += format(counter,'04b')
    if len(IV) > 12:
        IV = IV[len(IV)-12:]
    return IV

wybor = int(input("1 - ECB 2 - CBC 3 - CFB 4 - CTR\n"))
sciezka = input("podaj sciezke do pliku\n")


obrazek = Image.open(sciezka, "r")
obrazek = obrazek.convert('1')
format_pliku = obrazek.format

pbm_temp = io.BytesIO()
obrazek.save(pbm_temp, 'PPM')
data = pbm_temp.getvalue()


pbm_temp.close()
obrazek_lista = pbm_conv(data)
width = obrazek_lista[0]
height = obrazek_lista[1]


obrazek_zasz_conv1 = Image.new('1', (width, height))
obrazek_lista2 = ''.join(obrazek_lista[-1])

lista_pikseli = []
for liczba in obrazek_lista2:
    if liczba == '1':
        nowa_liczba = 0
    else:
        nowa_liczba = 255
    lista_pikseli.append(nowa_liczba)

obrazek_zasz_conv1.putdata(lista_pikseli)
obrazek_zasz_conv1.save(sciezka[:-4]+"_niezasz.jpg", 'JPEG')


tekst_jawny_lista = []
tekst_jawny_input = "".join(obrazek_lista[-1])

tekst_jawny_len = len(tekst_jawny_input)
if tekst_jawny_len % 12 != 0:
    tekst_jawny_len = tekst_jawny_len - tekst_jawny_len % 12

for i in range(0, tekst_jawny_len, 12):
    tekst_jawny_lista.append(tekst_jawny_input[i:i+12])


def xor(a, b):
    if a != b:
        return "1"
    else:
        return "0"

zasz_wiad_lista = []
tekst_jawny_xor = ["" for i in range(12)]

if wybor == 4:
    counter = 0
    nonce = secrets.token_bytes(1)
    IV = oblicz_IV(nonce, counter)

for tekst_jawny in tekst_jawny_lista:
    if wybor == 2:
        for i in range(0, 12):
            tekst_jawny_xor[i] = xor(IV[i], tekst_jawny[i])
        tekst_jawny = tekst_jawny_xor[:]
    if wybor == 3 or wybor == 4:
        tekst_jawny2 = tekst_jawny[:]
        tekst_jawny = IV[:]


    L0 = tekst_jawny[0:6]
    R0 = tekst_jawny[6:12]

    ER0 = ["" for i in range(8)]
    xor_klucz_ER0 = ["" for i in range(8)]
    R1_L2 = ["" for i in range(6)]

    for n in range(1, 8):

        for i in range(0, len(permutacja)):
            ER0[i] = R0[permutacja[i]]

        klucz_i = klucz[n:8] + klucz[0:n]

        for i in range(0, 8):
            xor_klucz_ER0[i] = xor(ER0[i], klucz_i[i])

        xor_klucz_ER0_a = xor_klucz_ER0[0:4]
        xor_klucz_ER0_b = xor_klucz_ER0[4:8]

        xor_klucz_ER0_a = "".join(xor_klucz_ER0_a)
        xor_klucz_ER0_b = "".join(xor_klucz_ER0_b)

        xor_klucz_ER0_a_int = int(xor_klucz_ER0_a, 2)
        xor_klucz_ER0_b_int = int(xor_klucz_ER0_b, 2)

        sbox1_ciag = sbox1[xor_klucz_ER0_a_int]
        sbox2_ciag = sbox2[xor_klucz_ER0_b_int]
        F = sbox1_ciag + sbox2_ciag

        for i in range(6):
            R1_L2[i] = xor(L0[i], F[i])
        L0 = R0[:]
        R0 = R1_L2[:]

    zasz_wiad = R0 + L0
    if wybor == 2:
        IV = zasz_wiad[:]
    if wybor == 3:
        zasz_wiad2 = ["" for i in range(12)]
        for i in range(0, 12):
            zasz_wiad2[i] = xor(tekst_jawny2[i], zasz_wiad[i])
        zasz_wiad = zasz_wiad2[:]
        IV = zasz_wiad[:]
    if wybor == 4:
        zasz_wiad2 = ["" for i in range(12)]
        for i in range(0, 12):
            zasz_wiad2[i] = xor(tekst_jawny2[i], zasz_wiad[i])
        zasz_wiad = zasz_wiad2[:]
        counter += 1
        IV = oblicz_IV(nonce,counter)

    zasz_wiad = "".join(zasz_wiad)
    zasz_wiad_lista.append(zasz_wiad)


obrazek_zaszyfr_ciag = "".join(zasz_wiad_lista)

obrazek_zasz_conv = Image.new('1', (width, height))
lista_pikseli = []
for liczba in obrazek_zaszyfr_ciag:
    if liczba == '1':
        nowa_liczba = 0
    else:
        nowa_liczba = 255
    lista_pikseli.append(nowa_liczba)

obrazek_zasz_conv.putdata(lista_pikseli)


obrazek_zasz_conv.save(sciezka[:-4]+"_zasz"+sciezka[-4:], format_pliku)
