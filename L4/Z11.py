import string
from itertools import product
import requests
import sys
import timeit
import threading
import time

FREQ = ['E', 'T', 'A', 'O', 'I', 'N', 'S', 'H', 'R', 'D', 'L', 'C', 'U', 'M', 'W', 'F', 'G', 'Y', 'P', 'B', 'V',
          'K', 'J', 'X', 'Q', 'Z']
najcz_ang = ('E', 'T', 'A', 'O', 'I', 'N')
najrz_ang = ('V', 'K', 'J', 'X', 'Q', 'Z')

odszyfrowane_slowo1 = ''
odszyfrowane_slowo2 = ''
r = requests.get("https://stepik.org/media/attachments/lesson/668860/dictionary.txt")
#print(r.text) #Wyświetli słownik
words = r.text
words_dict = dict()
lines = words.splitlines()
global max_dl_klucza

for line in lines:
    line.strip()
    words_dict[line] = line[0]
def strip_tekst(tekst):
    tekst_stripped = tekst.upper()
    return tekst_stripped

def znajdz_powt(tekst):
    sekwencje = {}
    n = len(tekst)
    for i in range(0, n - max_dl_klucza + 1):
        for dlugosc in range(3, max_dl_klucza):
            sekwencja = tekst[i:i + dlugosc]
            if sekwencja in sekwencje:
                sekwencje[sekwencja][0] += 1
                sekwencje[sekwencja].append(i)
            else:
                sekwencje[sekwencja] = [1, i]
    powt_sekw = {}
    for key in sekwencje:
        if sekwencje[key][0] > 1:
            powt_sekw[key] = sekwencje[key]
    return powt_sekw

def rozklad(liczba):
    czynniki = []
    dzielnik = 2
    while liczba > 1 and dzielnik <= max_dl_klucza:
        if liczba % dzielnik == 0:
            czynniki.append(dzielnik)
        dzielnik += 1
    return czynniki

def Kasinski(tekst):
    powt = znajdz_powt(tekst)
    dict_powt = {}
    for key in powt:
        dict_powt[key] = []
        for i in range(len(powt[key]) - 1, 0, -1):
            for j in range(i - 1, 0, -1):
                odstep = powt[key][i] - powt[key][j]
                dict_powt[key].append(odstep)

    dict_czynniki = {}

    for key in dict_powt:
        dict_czynniki[key] = []
        for value in dict_powt[key]:
            czynniki = rozklad(value)
            dict_czynniki[key].extend(czynniki)

    dict_count = {}

    for key in dict_czynniki:
        dict_count_values = {}
        for value in dict_czynniki[key]:
            if value not in dict_count_values.keys():
                dict_count_values[value] = 1
            else:
                dict_count_values[value] += 1
        dict_count_values = dict(sorted(dict_count_values.items(), key=lambda x: x[1]))
        dict_count[key] = dict_count_values

    wynik = []

    for key in dict_count:
        for value in dict_count[key]:
            wynik.append(value)

    wynik = set(wynik)
    wynik = list(wynik)
    wynik.sort()
    return wynik

def decrypt(M, K):
    index_przes = []
    for litera in K:
        ascii_litera = ord(litera)
        if 64 < ascii_litera < 91:
            kod_litera = ascii_litera - 65
            index_przes.append(kod_litera)
        elif 96 < ascii_litera < 123:
            kod_litera = ascii_litera - 97
            index_przes.append(kod_litera)
    n = len(index_przes)
    i = 0
    slowo = []
    for litera in M:
        ascii_litera = ord(litera)
        if 64 < ascii_litera < 91:
            kod_litera = ascii_litera - 65
            kod_litera = (kod_litera - index_przes[i]) % 26
            ascii_litera = kod_litera + 65
            i = (i + 1) % n
        elif 96 < ascii_litera < 123:
            kod_litera = ascii_litera - 97
            kod_litera = (kod_litera - index_przes[i]) % 26
            ascii_litera = kod_litera + 97
            i = (i + 1) % n
        slowo.append(chr(ascii_litera))
    slowo = ''.join(slowo)
    return slowo

def decrypt_list(powt):
    powt_dict_subst = {litera: 0 for litera in FREQ[::-1]}
    dec_slowo = ''
    for litera in string.ascii_uppercase:
        if inp == '1':
            dec_slowo = decrypt(powt, litera)
        if inp == '2':
            dec_slowo = xor_decoder(litera.lower(), powt)
        powt_dict_subst[litera] = dec_slowo
    return powt_dict_subst

def analiza_czest(tekst):
    czest_dict = {litera: 0 for litera in FREQ[::-1]}
    analiza_czest = {'najcz': 0, 'najrz': 0}
    for litera in tekst:
        if inp == '1':
            if 64 < ord(litera) < 91 or 96 < ord(litera) < 123:
                czest_dict[litera.upper()] = czest_dict[litera.upper()] + 1
        if inp == '2':
            if 64 < litera < 91 or 96 < litera < 123:
                czest_dict[chr(litera).upper()] = czest_dict[chr(litera).upper()] + 1
    dict_sort = sorted(czest_dict.items(), key=lambda x: x[1], reverse=True)
    najcz_szyfr = dict_sort[0:6]
    najrz_szyfr = dict_sort[-6:]

    najrz_szyfr = dict(najrz_szyfr)
    najcz_szyfr = dict(najcz_szyfr)

    for litera in najcz_szyfr:
        if litera in najcz_ang:
            analiza_czest['najcz'] = analiza_czest['najcz'] + 1

    for litera in najrz_szyfr:
        if litera in najrz_ang:
            analiza_czest['najrz'] = analiza_czest['najrz'] + 1

    czestotliwosc = analiza_czest['najrz'] + analiza_czest['najcz']
    return czestotliwosc

def zbadaj_czestotliwosc(lista_ciagow_tekst):
    for ciag in lista_ciagow_tekst:
        lista_ciagow_tekst[ciag] = analiza_czest(lista_ciagow_tekst[ciag])
    lista_ciagow_tekst = sorted(lista_ciagow_tekst.items(), key=lambda x: x[1], reverse=True)
    return lista_ciagow_tekst

def build_substrings(tekst, value):
    powt_dict = {key: [] for key in range(0, value)}
    for i in range(0, len(tekst)-value + 1, value):
        for j in range(i, value+i):
            powt_dict[j % value].extend(tekst[j])
    for key in powt_dict:
        powt_dict[key] = ''.join(powt_dict[key])
    return powt_dict

def build_lista(tekst_stripped, dl_klucza):
    lista_liter = []
    powt_dict = build_substrings(tekst_stripped, dl_klucza)
    for key in powt_dict:
        powt_dict_subst = decrypt_list(powt_dict[key])
        lista_liter.append(zbadaj_czestotliwosc(powt_dict_subst))
    return lista_liter

def decryptFun(M1, M2, key):
    if inp == '1':
        tekst_decrypt1 = decrypt(M1, key)
    else:
        tekst_decrypt1 = xor_decoder(M1, key.lower()).decode('utf-8')
    if tekst_decrypt1.upper() in words_dict.keys():
        if inp == '1':
            tekst_dekrypt2 = decrypt(M2, key)
        else:
            tekst_dekrypt2 = xor_decoder(M2, key.lower()).decode('utf-8')
        if tekst_dekrypt2.upper() in words_dict.keys():
            print(tekst_decrypt1)
            print(tekst_dekrypt2)
            print(key.lower())
            global odszyfrowane_slowo1
            odszyfrowane_slowo1 = tekst_decrypt1
            global odszyfrowane_slowo2
            odszyfrowane_slowo2 = tekst_dekrypt2
            return True
    return False

def maxDlKlucza(M1):
    if len(M1) < 9:
        global max_dl_klucza
        max_dl_klucza = len(M1)
    else:
        max_dl_klucza = 9
    return max_dl_klucza

def Vigenere(M1,M2):
    tekst = M1 + M2
    tekst_stripped = ''
    if inp == '1':
        tekst_stripped = strip_tekst(tekst)
    if inp == '2':
        tekst_stripped = tekst
    powtorzenia = Kasinski(tekst_stripped)
    keys_not_dict = []
    if not powtorzenia and len(M1) not in powtorzenia:
        powtorzenia = [len(M1)]
    for value in powtorzenia:
        lista_liter = build_lista(tekst_stripped, value)
        for indexes in product(range(0, 26), repeat=value):
            key = ''
            for i in range(0, value):
                key += lista_liter[i][indexes[i]][0]
            if key in words_dict.keys():
                if decryptFun(M1, M2, key):
                    return
            else:
                keys_not_dict.append(key)
    for key in keys_not_dict:
        if decryptFun(M1, M2, key):
            return
    global odszyfrowane_slowo1
    odszyfrowane_slowo1 = ''
    global odszyfrowane_slowo2
    odszyfrowane_slowo2 = ''
    return


def xor_decoder(bytes_key, M):
    zipped_text = list(zip(M, bytes_key))
    list_words_ord_trans = bytearray()
    for a, b in zipped_text:
        elem = ord(a) ^ ord(b)
        list_words_ord_trans.append(elem)
    bytes_o = bytes(list_words_ord_trans)
    return bytes_o

def skutecznoscFun():
    count = 0
    dlug = 2 * len(odszyfrowane_slowo1)
    if dlug == 0:
        return
    for i in range(0, len(odszyfrowane_slowo1)):
        if odszyfrowane_slowo1[i] == wlas_tekst1[i]:
            count += 1
    for i in range(0, len(odszyfrowane_slowo2)):
        if odszyfrowane_slowo2[i] == wlas_tekst2[i]:
            count += 1
    skutecznosc_krypto = (count / dlug)*100
    print(skutecznosc_krypto)
    return skutecznosc_krypto


M1 = ''
M2 = ''
inp = input("1 - Vigenere 2 - Xor\n")
measure = input("Mierzyc czas? 1 - tak 2 - nie\n")
skutecznosc = input("Ocena skutecznosci? 1 - tak 2 - nie\n")
plik = input("Plik wczytanie/zapis? 1 - tak 2 - nie\n")
if plik == '1':
    wynik = "dlugosc, czas, skutecznosc\n"
    file = open("zaszyfr_tekst.txt", "br")
    file2 = open("wyniki_krypto.txt", "w")
    file2.write(wynik)
    while True:
        data = []
        for i in range(0,4):
            line = file.readline().strip()
            if not line:
                break
            data.append(line)
        if len(data) < 4:
            break
        wlas_tekst1 = data[0].decode('utf-8')
        wlas_tekst2 = data[1].decode('utf-8')
        if inp == '1':
            M1 = data[2].decode('utf-8')
            M2 = data[3].decode('utf-8')
        if inp == '2':
            M1 = data[2].decode('unicode-escape')
            M2 = data[3].decode('unicode-escape')
        max_dl_klucza = maxDlKlucza(M1)
        time_start = time.time()
        thread = threading.Thread(target=Vigenere(M1,M2))
        thread.start()
        thread.join()
        skutecznosc_krypto = skutecznoscFun()
        time_finish = time.time()
        running_time = time_finish - time_start
        dlugosc_slowa = len(M1)
        file2.write(str(dlugosc_slowa) + ","+str(running_time) + ","+str(skutecznosc_krypto)+"\n")
    file.close()
    file2.close()

else:
    if skutecznosc == '1':
        print("Wprowadz tekst do analizy skutecznosci")
        wlas_tekst1 = input()
        wlas_tekst2 = input()
    print("wprowadz tekst do odszyfrowania")
    if inp == '1':
        M1 = sys.stdin.buffer.readline().strip().decode('utf-8')
        M2 = sys.stdin.buffer.readline().strip().decode('utf-8')
    if inp == '2':
        M1 = sys.stdin.buffer.readline().strip().decode('unicode-escape')
        M2 = sys.stdin.buffer.readline().strip().decode('unicode-escape')
    max_dl_klucza = maxDlKlucza(M1)
    if measure == '1':
        running_time = timeit.timeit("Vigenere(M1,M2)", globals=globals(), number=1)
        print(running_time)
    else:
        Vigenere(M1,M2)
if skutecznosc == '1':
    skutecznosc_krypto = skutecznoscFun()

